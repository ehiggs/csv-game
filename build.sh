#!/bin/bash

main() {
  local directory=$1; shift

  local script_base=$(dirname $(readlink -f $0))
  pushd "$script_base"
  pushd "$directory"
  ./build.sh
  popd
  popd
}

main "$@"
