#!/bin/bash

source  ../common/timer.sh
(cd csv && go build -o csv)
timer ../results.csv golang csv fieldcount "./csv/csv < ../build/hello.csv"
timer ../results.csv golang csv empty "./csv/csv < ../build/empty.csv"
