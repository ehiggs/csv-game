extern crate csv;

fn main() {
    let fpath = ::std::env::args().nth(1).unwrap();
    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_path(fpath)
        .unwrap();
    let mut sum = 0;
    for record in rdr.records() {
        let record = record.unwrap();
        sum += record.len();
    }
    println!("{}", sum);
}
