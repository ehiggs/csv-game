use std::io::BufReader;
use std::io::BufRead;
use std::fs::File;
use lalrpop_util::lalrpop_mod;

lalrpop_mod!(pub csv);

fn main() {
    let fpath = ::std::env::args().nth(1).unwrap();
    let f = File::open(fpath).unwrap();
    let file = BufReader::new(&f);
    let mut sum = 0;
    let parser = csv::RecordParser::new();
    for line in file.lines() {
        let l = line.unwrap();
        let rec = parser.parse(&l).unwrap();
        sum += rec.len();
    }
    println!("{}", sum);
}
