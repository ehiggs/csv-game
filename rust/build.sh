#!/bin/bash
source ../common/timer.sh
# nom-reader skipped as it's wayyyy out of date
for SCRIPT in csvreader quick-reader peg-reader lalr-reader; do 
  echo $SCRIPT
  (cd $SCRIPT && cargo build --release)
done
timer ../results.csv rust csvreader fieldcount "./csvreader/target/release/csvreader ../build/hello.csv"
timer ../results.csv rust csvreader empty "./csvreader/target/release/csvreader ../build/empty.csv"
timer ../results.csv rust quick-reader fieldcount "./quick-reader/target/release/quick-reader ../build/hello.csv"
timer ../results.csv rust quick-reader empty "./quick-reader/target/release/quick-reader ../build/empty.csv"
timer ../results.csv rust peg-reader fieldcount "./peg-reader/target/release/peg-reader ../build/hello.csv"
timer ../results.csv rust peg-reader empty "./peg-reader/target/release/peg-reader ../build/empty.csv"
timer ../results.csv rust lalr-reader fieldcount "./lalr-reader/target/release/lalr-reader ../build/hello.csv"
timer ../results.csv rust lalr-reader empty "./lalr-reader/target/release/lalr-reader ../build/empty.csv"
