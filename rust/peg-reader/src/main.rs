use std::fs::File;
use std::io::Read;

peg::parser! {
    grammar record_parser() for str {
        rule field() -> u32
            = "\"" [^'"']*  "\"" { 1 }
            / [^','|'\n']* { 1 }

        pub rule record() -> u32
            = f:field() ++ "," { f.into_iter().sum() }

        pub rule file() -> u32
            = r:(r:record() "\n" { r } )* { r.into_iter().sum() }
    }
}

fn main() {
    let fpath = ::std::env::args().nth(1).unwrap();
    let mut f = File::open(fpath).unwrap();
    let mut text = String::new();
    f.read_to_string(&mut text).unwrap();
    let sum = record_parser::file(&text).unwrap();
    println!("{}", sum);
}

#[test]
fn test_hello() {
    assert_eq!(
        record_parser::record(r#"hello,","," ",world,"!""#).unwrap(),
        5
    );
    assert_eq!(
        record_parser::file(
            r#"hello,","," ",world,"!"
hello,","," ",world,"!"
"#
        )
        .unwrap(),
        10
    );
}
