#!/bin/bash
source ../common/timer.sh
mvn package
timer ../results.csv java BeanIOCsv fieldcount "./BeanIOCsv < ../build/hello.csv"
timer ../results.csv java BeanIOCsv empty "./BeanIOCsv < ../build//empty.csv"
timer ../results.csv java CommonsCsv fieldcount "./CommonsCsv < ../build//hello.csv"
timer ../results.csv java CommonsCsv empty "./CommonsCsv < ../build//empty.csv"
timer ../results.csv java CSVeedCsv fieldcount "./CSVeedCsv < ../build//hello.csv"
timer ../results.csv java CSVeedCsv empty "./CSVeedCsv < ../build//empty.csv"
timer ../results.csv java JavaCsv fieldcount "./JavaCsv < ../build//hello.csv"
timer ../results.csv java JavaCsv empty "./JavaCsv < ../build//empty.csv"
timer ../results.csv java OpenCsv fieldcount "./OpenCsv < ../build//hello.csv"
timer ../results.csv java OpenCsv empty "./OpenCsv < ../build//empty.csv"
timer ../results.csv java UnivocityCsv fieldcount "./UnivocityCsv < ../build//hello.csv"
timer ../results.csv java UnivocityCsv empty "./UnivocityCsv < ../build//empty.csv"
